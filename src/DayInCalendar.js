import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class  DayInCalendar extends React.Component {
    state = {
        day: this.props.day
      }
    
    render() {
        let className = "square";
        if (this.state.day.isToday) {
            className = "square_today";
          }
        if (this.state.day.isWend){
            className += "_wend";
        }
      return (
        <button className={className} 
            onClick={() => {
                console.log(this.state.day)
                let newApoiment = prompt('Escribe tu cita para el ' + this.props.day.number)
                this.props.day.apoiments[0] = newApoiment
                this.setState( 
                    this.state.day = this.props.day
                )
            }
        }>
            {this.state.day.number + "\t"}
            {this.state.day.apoiments[0]?this.state.day.apoiments[0]:""}
        </button>
      );
    }
  }

  // ========================================

export default DayInCalendar;