import React from 'react';
import './index.css';
import moment from 'moment'
import DayInCalendar from './DayInCalendar';

class WeekDayTitle extends React.Component{
    render() {
        return (
            <button className="week-day">
                {this.props.title}
            </button>
        );
      }
}

class Board extends React.Component {
    state = {
        dateObject: moment(),
        allmonths :moment.months()
      };
    
    renderSquare(day) {
        return <DayInCalendar day={day} />;
    }

    renderWeekDay(day) {
        return <WeekDayTitle title={day} />;
      }

      firstDayOfMonth = () => {
        let firstDay = moment(this.state.dateObject)
                     .startOf("month")
                     .format("d"); 
       return firstDay;
    };

    calculateCalendarState() {
        let blanks = [];
        for (let i = 0; i < this.firstDayOfMonth(); i++) {
            let thisMoment = moment(this.firstDayOfMonth()).add('day', -(this.firstDayOfMonth()) - 1 + i );
            blanks.push({
            moment: thisMoment,
            number: thisMoment.date(),
            apoiments: [],
            isToday: false,
            isWend: thisMoment.day()===0 || thisMoment.day()===1
          });
        }
        let daysInMonth = [];
        for (let d = 1; d <= moment().daysInMonth(); d++) {
            let thisMoment = moment(this.firstDayOfMonth()).add('day', d-1 );
            daysInMonth.push({
                moment: thisMoment,
                number: thisMoment.date(),
                apoiments: [],
                isToday: d===this.state.dateObject.date(),
                isWend: thisMoment.day()===2 || thisMoment.day()===1
                });
            }
        let totalSlots = [...blanks, ...daysInMonth];

        let rows = [];
        let cells = [];
        totalSlots.forEach((row, i) => {
            if (i % 7 !== 0) {
              cells.push(row);
            } else {
              rows.push(cells);
              cells = [];
              cells.push(row);
            }
            if (i === totalSlots.length - 1) {
              rows.push(cells);
            }
          });
          console.log (rows);
          console.log (totalSlots);

          const listest = rows.map((str) => <div>{
                 str.map((day) => this.renderSquare(day))
        }</div> )
        return listest;
    }

    render() {
    //calculateCalendarState();  
      return (
        <div>
          <div className="board-row">
            {this.renderWeekDay("Sunday")}
            {this.renderWeekDay("Monday")}
            {this.renderWeekDay("Tuesday")}
            {this.renderWeekDay("Wednesday")}
            {this.renderWeekDay("Thursday")}
            {this.renderWeekDay("Friday")}
            {this.renderWeekDay("Saturday")}
          </div>
          <div className="board-row">
            {this.calculateCalendarState()}
          </div>
        </div>
      );
    }
  }

  export default Board