import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Board from './Board';
    
  class Calendar extends React.Component {
    render() {
      return (
        <div className="calendar">
          <div className="calendar">
            <Board />
          </div>
        </div>
      );
    }
  }
  
  // ========================================
  
  ReactDOM.render(
    <Calendar />,
    document.getElementById('root')
  );
  